import { Enemy } from "../Enemy";
import { Hero } from "../Hero"

export class Human extends Hero{

  protected race: string = "Human";


  public attack(enemy: Enemy) {

    let bonus = enemy.getFly() ? 0.9 : 1.1;

    let strength = this.getHitStrength() * bonus;

    return strength;
  }


  public defend(damage: number): number {

    this.setHealth(damage);
    
    return damage;
  }

}
