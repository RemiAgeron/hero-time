import { Enemy } from "../Enemy";
import { Hero } from "../Hero"

export class Dwarf extends Hero{

  protected race: string = "Dwarf";


  public attack(_: Enemy) {

    let strength = this.getHitStrength();

    return strength;
  }


  public defend(damage: number): number {

    let bonus = Math.floor(Math.random() * 5) == 0 ? 0.5 : 1;

    this.setHealth(damage * bonus);

    return damage *= bonus;
  }

}
