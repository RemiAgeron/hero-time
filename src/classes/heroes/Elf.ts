import { Enemy } from "../Enemy";
import { Hero } from "../Hero"

export class Elf extends Hero{

  protected race: string = "Elf";


  public attack(enemy: Enemy) {

    let bonus = enemy.getFly() ? 1.1 : 0.9;

    let strength = this.getHitStrength() * bonus;

    return strength;
  }


  public defend(damage: number): number {

    this.setHealth(damage);
    
    return damage;
  }

}
