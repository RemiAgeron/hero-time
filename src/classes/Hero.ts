import { Character } from "./Character";

export class Hero extends Character{

  protected race: string;

  public setRace(input: string): void {
    this.race = input;
  }

  public getRace(): string {
    return this.race;
  }

}
