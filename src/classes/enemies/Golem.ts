import { Enemy } from "../Enemy"

export class Golem extends Enemy{

  public attack(): number {
    return this.getHitStrength();
  }


  public defend(damage: number): number {

    let rand = Math.floor(Math.random() * 2);

    this.setHealth(damage * rand);

    return damage *= rand;
  }

}