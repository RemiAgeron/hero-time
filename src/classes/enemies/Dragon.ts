import { Enemy } from "../Enemy";

export class Dragon extends Enemy{

  public fly(): number {

    this.setTakeoff(false);

    this.setFly(true);

    return 0;
  }

  public setFly(bool: boolean): void {
    this.flying = bool;
  }

  public setTakeoff(bool: boolean): void {
    this.takeoff = bool;
  }


  public attack(): number {
    return this.getFly() ? this.attackFromSky() : (this.getTakeoff() ? this.fly() : this.attackFromGround());
  }

  public attackFromGround(): number {
    this.setTakeoff(true);
    return this.getHitStrength();
  }

  public attackFromSky(): number {
    this.setFly(false);
    return this.getHitStrength() * 1.1;
  }


  public defend(damage: number): number {

    let bonus = this.getFly() ? 0.4 : 0.5;

    this.setHealth(damage * bonus);

    return damage *= bonus;
  }

}
