import { Enemy } from "../Enemy"

export class Berserker extends Enemy{

  public attack(): number {
    return this.getHitStrength();
  }


  public defend(damage: number): number {

    this.setHealth(damage * 0.7);

    return damage *= 0.7;
  }

}