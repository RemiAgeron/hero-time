import { Enemy } from "../Enemy"

export class Werewolf extends Enemy{

  public attack(): number {
    return this.getHitStrength();
  }


  public defend(damage: number): number {

    this.setHealth(damage * 0.5);

    return damage *= 0.5;
  }

}