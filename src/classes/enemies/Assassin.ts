import { Enemy } from "../Enemy"

export class Assassin extends Enemy{

  public attack(): number {

    let currentStrength = this.getHitStrength();

    this.setHitStrength(1.1);

      return currentStrength;
  }


  public defend(damage: number): number {

    this.setHealth(damage);

    return damage;
  }

}
