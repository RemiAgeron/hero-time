import { Stats } from "webpack";

export class Character {

  constructor(name: string, health: number, hitStrength:number){
    this.name = name;
    this.health = health;
    this.hitStrength = hitStrength;
    this.lvl = 1;
    this.xp = 0;
    this.healthHistory = health;
  }

  protected name: string;
  protected health: number;
  protected hitStrength: number;
  protected lvl: number;
  protected xp: number;
  protected healthHistory: number;
  

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getHealth(): number {
    return this.health;
  }

  public setHealth(damage: number): void {
    this.health = Math.round((this.health - damage) * 10) / 10;
    if(this.health < 0) {
      this.health = 0;
    }
  }
  public addHealth(heal: number): void {
    this.health += heal;
  }


  public getHitStrength(): number {
    return this.hitStrength * this.getLvl();
  }

  public setHitStrength(bonus: number): void {
    this.hitStrength = Math.round((this.hitStrength * bonus) * 10) / 10;
  }

  public getLvl(): number {
    return this.lvl;
  }

  public setLvl(): void {
    this.lvl++;
  }

  public getXp(): number {
    return this.xp;
  }
  public getStats(): Array<number> {
      return [this.health, this.hitStrength, this.lvl,this.xp];
  }

  public setXp(): void {
    this.xp += 2;
    if (this.xp >= 10) {
      this.setLvl();
      this.xp = 0;
    }
  }

  public die(): number{
    return this.healthHistory * 0.1;
  }
}
