export class Battle {

  protected pOne: any;
  protected pTwo: any;
  protected history: Array<string> = [];

  constructor(pOne: any, pTwo: any) {
    this.pOne = pOne;
    this.pTwo = pTwo;
  }

  public getHistory(): Array<string> {
    return this.history;
  }

  public addHistory(result: string): void {
    this.history.unshift(result);
  }

  public fight(): void {

    let round = 1;
    let table = document.getElementById('round');
    let result = document.getElementById('result');
    table.textContent = null;

    var myInterval = setInterval(() => {
      
      let tr = document.createElement("tr");
      let tdOne = document.createElement("td");
      tdOne.textContent = "Round " + String(round);

      let tdTwo = document.createElement("td");
      let pOneAttack = Math.round(this.pOne.attack(this.pTwo) * 10) / 10;
      let pTwoDefend = Math.round(this.pTwo.defend(pOneAttack) * 10) / 10;
      tdTwo.appendChild(document.createTextNode(this.pOne.getName() + " fait " + String(pTwoDefend) + " de dégats à " + this.pTwo.getName()));

      if (this.pTwo.getHealth() <= 0) {
        tdTwo.appendChild(document.createElement("br"));
        tr.appendChild(tdOne);
        tr.appendChild(tdTwo);
        table.appendChild(tr);
        
        this.pOne.addHealth(this.pTwo.die());
        this.pOne.setXp();

        this.addHistory(this.pOne.getName() + " a vaincu " + this.pTwo.getName());

        let p = document.createElement("p");
        p.className = "result";
    
        this.getHistory().forEach(element => { 
          p.appendChild(document.createTextNode(element));
          p.appendChild(document.createElement("br"));
        });
    
        result.appendChild(p);

        clearInterval(myInterval);
      }

      tdTwo.appendChild(document.createElement("br"))
      let pTwoAttack = Math.round(this.pTwo.attack(this.pOne) * 10) / 10;
      let pOneDefend = Math.round(this.pOne.defend(pTwoAttack) * 10) / 10;
      tdTwo.appendChild(document.createTextNode(this.pTwo.getName() + " fait " + String(pOneDefend) + " de dégats à " + this.pOne.getName()));

      if (this.pOne.getHealth() <= 0) {
        tdTwo.appendChild(document.createElement("br"));
        tr.appendChild(tdOne);
        tr.appendChild(tdTwo);
        table.appendChild(tr);

        this.pTwo.addHealth(this.pOne.die());
        this.pTwo.setXp();

        this.addHistory(this.pTwo.getName() + " a vaincu " + this.pOne.getName());

        let p = document.createElement("p");
        p.className = "result";
    
        this.getHistory().forEach(element => { 
          p.appendChild(document.createTextNode(element));
          p.appendChild(document.createElement("br"));
        });
    
        result.appendChild(p);

        clearInterval(myInterval);
      }

      tr.appendChild(tdOne);
      tr.appendChild(tdTwo);
      table.appendChild(tr);

      round++;

    }, 1000);

  }

}

