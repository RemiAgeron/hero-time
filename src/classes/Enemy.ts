import { Character } from "./Character";

export class Enemy extends Character {

  protected flying: boolean = false;
  protected takeoff: boolean = false;

  public getFly(): boolean {
    return this.flying;
  }

  public getTakeoff(): boolean {
    return this.takeoff;
  }

}
