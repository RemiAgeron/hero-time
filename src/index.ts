import { Battle } from './classes/Battle';
import { Character } from './classes/Character';
import { Assassin } from './classes/enemies/Assassin';
import { Berserker } from './classes/enemies/Berserker';
import { Dragon } from './classes/enemies/Dragon';
import { Golem } from './classes/enemies/Golem';
import { Griffin } from './classes/enemies/Griffin';
import { Werewolf } from './classes/enemies/Werewolf';
import { Dwarf } from './classes/heroes/Dwarf';
import { Elf } from './classes/heroes/Elf';
import { Human } from './classes/heroes/Human';

import './style.css';

import human_img = require('./assets/human.png');
import elf_img = require('./assets/elf.png');
import dwarf_img = require('./assets/dwarf.png');
import dragon_img = require('./assets/dragon.png');
import griffin_img = require('./assets/griffin.png');
import berserker_img = require('./assets/berserker.png');
import werewolf_img = require('./assets/werewolf.png');
import assassin_img = require('./assets/assassin.png');
import golem_img = require('./assets/golem.png');

let images = [
    { name: 'Human', img: human_img },
    { name: 'Elf', img: elf_img },
    { name: 'Dwarf', img: dwarf_img },
    { name: 'Dragon', img: dragon_img },
    { name: 'Griffin', img: griffin_img },
    { name: 'Berserker', img: berserker_img },
    { name: 'Werewolf', img: werewolf_img },
    { name: 'Assassin', img: assassin_img },
    { name: 'Golem', img: golem_img }
];

function stats(playerOne:Character, playerTwo:Character){

  let playerOneStats = document.getElementById('playerOne-stats');
  playerOneStats.textContent = null;

  playerOne.getStats().forEach(element => { 
  let td = document.createElement("td");
    td.textContent = String(element);
    playerOneStats.appendChild(td);
  });

  let playerTwoStats = document.getElementById('playerTwo-stats');
  playerTwoStats.textContent = null;

  playerTwo.getStats().forEach(element => {
  let td = document.createElement("td");
    td.textContent = String(element);
    playerTwoStats.appendChild(td);
  });

}

function userNumPrompt(variable: number, message: string): number {
  variable = Math.round(parseFloat(prompt(message)) * 10) / 10;
  while (isNaN(variable) || variable < 0) () => variable = Math.round(parseFloat(prompt(message)) * 10) / 10;
  return variable;
}

let title = 'Hello TypeScript' as string;

let fight = document.getElementById("fight");
let round = document.getElementById("round");
let result = document.getElementById("result");
let new_fight = document.getElementById("new-fight");

round.hidden = true;
result.hidden = true;
new_fight.hidden = true;

let playerOne_left_arrow = document.getElementById("playerOne-left-arrow");
let playerOne_right_arrow = document.getElementById("playerOne-right-arrow");
let playerTwo_left_arrow = document.getElementById("playerTwo-left-arrow");
let playerTwo_right_arrow = document.getElementById("playerTwo-right-arrow");
let playerOne_arrows = document.getElementById("playerOne-arrows");
let playerTwo_arrows = document.getElementById("playerTwo-arrows");

playerOne_arrows.hidden = false;
playerTwo_arrows.hidden = false;

let playerOne: Character;

let playerOne_health: number = 100;
let playerOne_health_btn = document.getElementById("playerOne-btn-health");
playerOne_health_btn.addEventListener("click", () => {
  playerOne_health = userNumPrompt(playerOne_health, "Les points de vie du Premier Joueur (nombre) : ");
  playerOne = createPlayer(images[playerOne_index].name, "Joueur 1", playerOne_health, playerOne_hitStrength);
  stats(playerOne, playerTwo);
});

let playerOne_hitStrength: number = 20;
let playerOne_hitStrength_btn = document.getElementById("playerOne-btn-hitStrength");
playerOne_hitStrength_btn.addEventListener("click", () => {
  playerOne_hitStrength = userNumPrompt(playerOne_hitStrength, "Les points de force du Premier Joueur (nombre) : ");
  playerOne = createPlayer(images[playerOne_index].name, "Joueur 1", playerOne_health, playerOne_hitStrength);
  stats(playerOne, playerTwo);
});

let playerTwo: Character;

let playerTwo_health: number = 100;
let playerTwo_health_btn = document.getElementById("playerTwo-btn-health");
playerTwo_health_btn.addEventListener("click", () => {
  playerTwo_health = userNumPrompt(playerTwo_health, "Les points de vie du Second Joueur (nombre) : ");
  playerTwo = createPlayer(images[playerTwo_index].name, "Joueur 2", playerTwo_health, playerTwo_hitStrength);
  stats(playerOne, playerTwo);
});

let playerTwo_hitStrength: number = 20;
let playerTwo_hitStrength_btn = document.getElementById("playerTwo-btn-hitStrength");
playerTwo_hitStrength_btn.addEventListener("click", () => {
  playerTwo_hitStrength = userNumPrompt(playerTwo_hitStrength, "Les points de force du Second Joueur (nombre) : ");
  playerTwo = createPlayer(images[playerTwo_index].name, "Joueur 2", playerTwo_health, playerTwo_hitStrength);
  stats(playerOne, playerTwo);
});

var playerOne_index = 1;
playerOne = createPlayer(images[playerOne_index].name, "Joueur 1", playerOne_health, playerOne_hitStrength);

var playerTwo_index = 8;
playerTwo = createPlayer(images[playerTwo_index].name, "Joueur 2", playerTwo_health, playerTwo_hitStrength);

stats(playerOne, playerTwo);

document.getElementById('playerOne-name').textContent = "Joueur 1";

document.getElementById('playerTwo-name').textContent = "Joueur 2";

fight.addEventListener("click", () => {

  document.getElementById('playerOne-name').textContent = playerOne.getName();

  document.getElementById('playerTwo-name').textContent = playerTwo.getName();

  let battle = new Battle(playerOne, playerTwo);

  battle.fight();
  
  playerOne_arrows.hidden = true;
  playerTwo_arrows.hidden = true;
  fight.hidden = true;

  round.hidden = false
  result.hidden = false
  new_fight.hidden = false;

  stats(playerOne, playerTwo);

});

new_fight.addEventListener("click", () => {
  location.reload();
});

// button <--- du player One
playerOne_left_arrow.addEventListener("click", () => {

  if (playerOne_index > 0) {
    playerOne_index--;
  } else {
    playerOne_index = images.length - 1;
  }

  let player_img = document.getElementById('playerOne');
  player_img.setAttribute("style", "content: url(" + images[playerOne_index].img + ")");

  let player_class = document.getElementById('playerOne-class');
  player_class.innerText = images[playerOne_index].name;

  playerOne = createPlayer(player_class.innerText, "Joueur 1", playerOne_health, playerOne_hitStrength);

});

//button ---> du player One
playerOne_right_arrow.addEventListener("click", () => {

  if (playerOne_index < images.length - 1) {
    playerOne_index++;
  } else {
    playerOne_index = 0;
  }

  let player_img = document.getElementById('playerOne');
  player_img.setAttribute("style", "content: url(" + images[playerOne_index].img + ")");

  let player_class = document.getElementById('playerOne-class');
  player_class.innerText = images[playerOne_index].name;

  playerOne = createPlayer(player_class.innerText, "Joueur 1", playerOne_health, playerOne_hitStrength);

});

//button <--- du player Two
playerTwo_left_arrow.addEventListener("click", () => {

  if (playerTwo_index > 0) {
    playerTwo_index--;
  } else {
    playerTwo_index = images.length - 1;
  }

  let player_img = document.getElementById('playerTwo');
  player_img.setAttribute("style", "content: url(" + images[playerTwo_index].img + ")");

  let player_class = document.getElementById('playerTwo-class');
  player_class.innerText = images[playerTwo_index].name;

  playerTwo = createPlayer(player_class.innerText, "Joueur 2", playerTwo_health, playerTwo_hitStrength);

});

//button ---> du player Two
playerTwo_right_arrow.addEventListener("click",  () => {

  if (playerTwo_index < images.length - 1) {
    playerTwo_index++;
  } else {
    playerTwo_index = 0;
  }

  let player_img = document.getElementById('playerTwo');
  player_img.setAttribute("style", "content: url(" + images[playerTwo_index].img + ")");

  let player_class = document.getElementById('playerTwo-class');
  player_class.innerText = images[playerTwo_index].name;

  playerTwo = createPlayer(player_class.innerText, "Joueur 2", playerTwo_health, playerTwo_hitStrength);

});

function createPlayer(classe: string, name: string, health: number, hitStrength: number): Character {
  switch (classe) {
    case 'Human': {
      return new Human(name, health, hitStrength);
    }
    case 'Elf': {
      return new Elf(name, health, hitStrength);
    }
    case 'Dwarf': {
      return new Dwarf(name, health, hitStrength);
    }
    case 'Dragon': {
      return new Dragon(name, health, hitStrength);
    }
    case 'Golem': {
      return new Golem(name, health, hitStrength);
    }
    case 'Griffin': {
      return new Griffin(name, health, hitStrength);
    }
    case 'Berserker': {
      return new Berserker(name, health, hitStrength);
    }
    case 'Assassin': {
      return new Assassin(name, health, hitStrength);
    }
    case 'Werewolf': {
      return new Werewolf(name, health, hitStrength);
    }
  }
}
